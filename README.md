# IVT Covid Simulation

Based on the paper by TU Berlin [insert link] we are performing simulations at IVT with our MATSim simulations. This document shows the continuously updated results and the technical set up (see below).

## Reference data

Currently, we are running the following scenarios in 25% samples:

- Switzerland
- Île-de-France
- São Paulo
- San Francisco

For all cases reference data is available from [OpenCovid](https://github.com/open-covid-19/data).
For San Francisco we only have California reference data, so it should be
scaled down (TODO).

![](images/reference.png)

## Calibration

Currently, calibration is ongoing. At the moment a couple of runs with different
calibration parameters are running on Euler. The first results are shown
below.

![](images/calibration.png)

## Technical setup

There is a folder on Euler on the IVT group share at

`/cluster/work/ivt_vpl/covid`

It contains the following folders:

- `/scenarios` all existing scenarios in different sample rates
- `/run` the run data (= simulation results)
- `/code` all the code needed to run the simulatons
- `/scripts` to be run to prepare and perform the simulations
- `/java` contains a recent AdoptJDK java distribution for Java 11

The workflow is as follows:

- If necessary `01_update.sh` removes the `code` directory, sets it up anew and builds the necessary Java projects
- If necessary (new scenario) `02_prepare.sh` prepares all the scenarios (see below)
- `03_calibration.sh` performs calibration runs with different calibration factors on the scenarios

### Code

The code for Episim from TU Berlin is located at [here](https://github.com/matsim-org/matsim-episim), and
we use the `develop` branch. The code needed to prepare our scenarios for Episim is located in the
`eqasim` repository [here](https://github.com/eqasim-org/eqasim-java) in the `covid19` branch.

For scenario preparation, we have two run scripts:

- `org.eqasim.examples.covid19.RunConvertEvents` takes an events file and a schedule file and creates a new events file with all necessary public transit enter/exit events that are needed by Episim
- `org.eqasim.examples.covid19.RunCleanPopulation` takes a plans (population) file and outputs a copy of it with all plans removed such that only the agent attributes remain

Both scripts are called from the `02_prepare.sh` script on the scenarios and they create the files `/scenarios/XXX/covid_events.xml.gz` and `/scenarios/XXX/covid_plans.xml.gz`, which are later used by the Episim runs.

### Scenarios

Each scenario is currently structured as follows:

- `/scenarios/XXX/events.xml.gz` Events
- `/scenarios/XXX/plans.xml.gz` Plans
- `/scenarios/XXX/schedule.xml.gz` Transit schedule

## Reporting

Currently, all reporting and plotting is done via the notebook [Covid.ipynb](Covid.ipynb).
